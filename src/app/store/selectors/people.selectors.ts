import { createSelector, createFeatureSelector } from '@ngrx/store';
import { State } from '../reducers';

export const selectState = createFeatureSelector('people');

export const selectPeople = createSelector(selectState, (state: State) => {
  return state ? state.people : null;
});
