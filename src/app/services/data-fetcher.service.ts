import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { PeopleSelectors, LocationsSelectors } from '../store/selectors';
import { State } from '../store/reducers';
import { map, filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class DataFetcherService {
  getPeople() {
    return this.store.pipe(select(PeopleSelectors.selectPeople)).pipe(
      filter((people) => !!people),
      map((people) => Object.values(people))
    );
  }

  getLocations() {
    return this.store.pipe(select(LocationsSelectors.selectLocations)).pipe(
      filter((locations) => !!locations)
    );
  }

  constructor(private readonly store: Store<State>) {}
}
